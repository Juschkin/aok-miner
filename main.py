from configparser import ConfigParser
from selenium import webdriver
from selenium.webdriver import FirefoxOptions
from selenium.webdriver.common.by import By
import html_to_json
import csv


def get_info(output_json):
    """extract info from map View"""
    info = {}
    try:
        title = output_json["p"][0]["_value"]
        info["Name"] = title
    except:
        pass
    try:
        address = output_json["p"][1]["_value"]
        info["Adresse"] = address
    except:
        pass
    for i in range(2, len(output_json["p"])):
        if "span" in output_json["p"][i].keys():
            try:
                attr_name = output_json["p"][i]["span"][0]["_attributes"]["class"][0]
                if "a" in output_json["p"][i]["span"][1]:
                    attr_val = output_json["p"][i]["span"][1]["a"][0]["_value"]
                else:
                    attr_val = output_json["p"][i]["span"][1]["_value"]
                info[attr_name] = attr_val
            except:
                pass
    return info


def collect_page(browser, plz, ph, pd, aua):
    """collect all entries of a given zip"""
    url = f'https://www.pflege-navigator.de/index.php?module=careinstitution&action=list&multi={plz}&hiddenCoords=&id=&ambit=10&mainSearchTypePH=1&mainSearchTypePD=1&mainSearchTypeAUA=1&extSearchQbSort=&extSearchTBVersion=&extSearchSortierungSort=&extSearchSortierungSelect='
    browser.get(url)
    all_elements = browser.find_elements(By.CLASS_NAME, 'main-search')
    if not all_elements:
        print("No elements found")
        return []
    search_list = all_elements[0]
    raw_html = search_list.get_attribute('innerHTML')
    output_json = html_to_json.convert(raw_html)
    output_json = output_json['div']
    events = []
    for i in output_json:
        if i["_attributes"]["id"].startswith('PD') and pd:
            events.append(i["_attributes"]["onclick"])
        elif i["_attributes"]["id"].startswith('PH') and ph:
            events.append(i["_attributes"]["onclick"])
        elif i["_attributes"]["id"].startswith('AUA') and aua:
            events.append(i["_attributes"]["onclick"])
    collect = []
    for i in events:
        browser.execute_script(i)
        all_elements = browser.find_elements(By.CLASS_NAME, 'map_popup_wrapper')
        html = all_elements[0].get_attribute('innerHTML')
        output_json = html_to_json.convert(html)
        info = get_info(output_json)
        collect.append(info)
    print("Collected:", len(collect))
    print("------------------------------------------------")
    return collect


def format_entry(attributes, entry):
    formatted = {}
    for a in attributes:
        try:
            formatted[a] = entry[a]
        except KeyError:
            formatted[a] = " "
    return formatted


def run_browser():
    #load config file
    config_object = ConfigParser()
    config_object.read("config.ini")
    config_info = config_object["WEB SCRAPPER INFO"]
    output_file = config_info["output file"]
    ph = True
    if config_info["pflegeheime"] != "True":
        print("Pflegeheime False")
        ph = False
    pd = True
    if config_info["pflegedienste"] != "True":
        pd = False
    aua = True
    if config_info["angebote zur unterstuetzung im alltag"] != "True":
        aua = False
    visible_browser = True
    if config_info["browser visibility"] != "True":
        visible_browser = False

    opts = FirefoxOptions()
    if not visible_browser:
        opts.add_argument("--headless")
    browser = webdriver.Firefox(options=opts)
    with open('german_zips.txt', 'r') as f:
        zips = [line.strip() for line in f]
    attributes = ["Name", "address", "tel", "fax", "mail", "web"]
    all_entries = []
    for i in range(0, len(zips)):
        print(f"ZIP index: {i} ZIP: {zips[i]}")
        collect = collect_page(browser, zips[i], ph, pd, aua)
        # merge entries to one dict
        for entry in collect:
            f_entry = format_entry(attributes, entry)
            all_entries.append(f_entry)
        # save entry to csv
        try:
            with open(output_file, 'w') as csv_file:
                writer = csv.DictWriter(csv_file, fieldnames=attributes)
                writer.writeheader()
                for data in all_entries:
                    writer.writerow(data)
        except IOError:
            print("I/O error")


if __name__ == '__main__':
    run_browser()
