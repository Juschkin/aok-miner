# AOK Pflege-Navigator Miner

A very easy data scraping tool for the AOK Pflege-Navigator at the following website:
https://www.pflege-navigator.de/.
It contains nursing Homes, care services and offers for support in everyday life. 
Each Entry can contain the following information:  
- Name  
- Address
- Tel  
- Fax  
- Mail  
- Website

## Requirements

For this web scraper you need the following python libraries:
- Selenium >= 4.0.0
- html-to-json >= 2.0.0

## Quick start
- First define your zip codes in **german_zips.txt**, where you want to find the desired information.
This file is already filled with over 83.000 zip codes. But you are free to change this list.
- After that change **config.ini** for your needs.
- Then run the script: ```python3 main.py```



